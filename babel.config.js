module.exports = {
    "env": {
        "test": {
          "presets": ["@babel/preset-env"],
          "plugins": ["transform-export-extensions"],
          "only": [
            "./**/*.js",
            "node_modules/jest-runtime"
          ]
        }
      }
};
