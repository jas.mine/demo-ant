import { queryUser } from '@/services/api';

export default {
  namespace: 'userDetail',
  state: {
    data: {},
  },
  effects: {
    * query ({ payload }, { call, put }) {
      const response = yield call(queryUser, payload)

      yield put({
        type: 'save',
        payload: response,
      });
    },
  },

  reducers: {
      save(state, action) {
        return {
          ...state,
          data: action.payload,
        };
      },
  },
}
