import React, { Fragment, PureComponent } from 'react';
import { Input } from 'antd';
import PhoneInput from 'react-phone-number-input';
import styles from './PhoneView.less';
import 'react-phone-number-input/style.css';


class PhoneView extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            phone: "",
        }
    }

  render() {
    const { value, onChange } = this.props;
    let values = ['', ''];
    if (value) {
      values = value.split('-');
    }

    return (
      <Fragment>
        <PhoneInput
          placeholder="Enter phone number"
          value={this.state.phone}
          onChange={phone => this.setState({ phone })}
        />
      </Fragment>
    );
  }
}

export default PhoneView;
