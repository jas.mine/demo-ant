import React, { Component, Fragment } from 'react';
import { formatMessage, FormattedMessage } from 'umi/locale';
import { Form, Input, Upload, Select, Button, Row, Col, Checkbox } from 'antd';
import { connect } from 'dva';
import styles from './BaseView.less';
import TimezoneView from './TimezoneView';
import PhoneView from './PhoneView';
// import { getTimeDistance } from '@/utils/utils';

const FormItem = Form.Item;
const CheckboxGroup = Checkbox.Group;
const { Option } = Select;

// 头像组件 方便以后独立，增加裁剪之类的功能
const AvatarView = ({ avatar }) => (
  <Fragment>
    <div className={styles.avatar_title}>
      <FormattedMessage id="app.settings.basic.avatar" defaultMessage="Avatar" />
    </div>
    <div className={styles.avatar}>
      <img src={avatar} alt="avatar" />
    </div>
    <Upload fileList={[]}>
      <div className={styles.button_view}>
        <Button icon="upload" />
      </div>
    </Upload>
  </Fragment>
);
const validatorPhone = (rule, value, callback) => {
  const values = value.split('-');
  if (!values[0]) {
    callback('Please input your area code!');
  }
  if (!values[1]) {
    callback('Please input your phone number!');
  }
  callback();
};

@connect(({ user }) => ({
  currentUser: user.currentUser,
}))
@Form.create()
class BaseView extends Component {
  componentDidMount() {
    this.setBaseInfo();
  }

  setBaseInfo = () => {
    const { currentUser, form } = this.props;
    Object.keys(form.getFieldsValue()).forEach(key => {
      const obj = {};
      obj[key] = currentUser[key] || null;
      form.setFieldsValue(obj);
    });
  };

  getAvatarURL() {
    const { currentUser } = this.props;
    if (currentUser.avatar) {
      return currentUser.avatar;
    }
    const url = 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png';
    return url;
  }

  getViewDom = ref => {
    this.view = ref;
  };

  render() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    const checkboxOptions = [
        { label: 'Administrator', value: '0' },
        { label: 'Scheduler', value: '1' },
        { label: 'Field Agent', value: '2',},
    ];

    return (
      <div className={styles.baseView} ref={this.getViewDom}>
        <div className={styles.right}>
          <AvatarView avatar={this.getAvatarURL()} />
        </div>
        <div className={styles.left}>
          <Form layout="vertical" onSubmit={this.handleSubmit} hideRequiredMark>
            <Row gutter={{ md: 8, lg: 24 }}>
              <Col md={12} sm={24}>
                <FormItem label={formatMessage({ id: 'app.settings.basic.firstName' })}>
                  {getFieldDecorator('firstName', {
                    rules: [
                      {
                        required: true,
                        message: formatMessage({ id: 'app.settings.basic.firstNameMessage' }, {}),
                      },
                    ],
                })(<Input className={styles.input} />)}
                </FormItem>
              </Col>
              <Col md={12} sm={24}>
                <FormItem label={formatMessage({ id: 'app.settings.basic.lastName' })}>
                  {getFieldDecorator('lastName', {
                    rules: [
                      {
                        required: true,
                        message: formatMessage({ id: 'app.settings.basic.lastNameMessage' }, {}),
                      },
                    ],
                })(<Input className={styles.input} />)}
                </FormItem>
              </Col>
            </Row>
            <FormItem label={formatMessage({ id: 'app.settings.basic.phone' })}>
              {getFieldDecorator('phone', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.settings.basic.phone-message' }, {}),
                  },
                  { validator: validatorPhone },
                ],
              })(<PhoneView />)}
            </FormItem>
            <FormItem label={formatMessage({ id: 'app.settings.basic.email' })}>
              {getFieldDecorator('email', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.settings.basic.email-message' }, {}),
                  },
                ],
              })(<Input className={styles.input} />)}
            </FormItem>
            <FormItem label={formatMessage({ id: 'app.settings.basic.timezone' })}>
              {getFieldDecorator('timezone', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.settings.basic.timezone-message' }, {}),
                  },
                ],
              })(<TimezoneView className={styles.input} />)}
            </FormItem>
            <FormItem label={formatMessage({ id: 'app.settings.basic.language' })}>
              <Select
                defaultValue="English"
                className={styles.selectStyle}
              >
                <Option value="English">English</Option>
                <Option value="French">Français</Option>
                <Option value="Chinese">中国</Option>
              </Select>
            </FormItem>
            <FormItem label={formatMessage({ id: 'app.settings.basic.role' })}>
              <CheckboxGroup options={checkboxOptions} defaultValue={['2']} />
            </FormItem>
            <Button type="primary" style={{ float: "right"}}>
              <FormattedMessage
                id="app.settings.basic.update"
                defaultMessage="Update user settings"
              />
            </Button>
          </Form>
        </div>
      </div>
    );
  }
}

export default BaseView;
