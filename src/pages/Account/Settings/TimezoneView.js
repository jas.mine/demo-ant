import React, { Component } from 'react';
import { Select } from 'antd';
import timezone from '../../../../mock/geographic/timezone.json';
import styles from './TimezoneView.less';

const { Option } = Select;
class TimezoneView extends Component {
  state = {
    data: timezone,
    value: "China Standard Time",
  }

  handleSearch = (value) => {
    fetch(value, data => this.setState({ data }));
  }

  handleChange = (value) => {
    this.setState({ value });
  }

  render() {
    const options = this.state.data.map(d => <Option key={d.value}>{d.text}</Option>);
    return (
      <Select
        className={styles.selectTimeStyle}
        showSearch
        value={this.state.value}
        defaultActiveFirstOption={false}
        onSearch={this.handleSearch}
        onChange={this.handleChange}
        notFoundContent={null}
      >
        {options}
      </Select>
    );
  }
}

export default TimezoneView;
