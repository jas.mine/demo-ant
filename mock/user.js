// 代码中会兼容本地 service mock 以及部署站点的静态数据
const queryArray = (array, key) => {
  if (!(array instanceof Array)) {
    return null
  }
  const data = array.find(arr =>  arr.key === parseInt(key));
  if (data) {
    return data
  }
  return null
}

const database = [
    {
        key: 0,
        firstName: `Henry`,
        lastName: `Tran`,
        email: `hery@ftvlabs.com`,
        phone: `65-9016 1920`,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date(),
        createdAt: new Date("2018-10-18"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 1,
        firstName: `Dan`,
        lastName: `Nguyen`,
        email: `nguyenducanh2210@gmail.com`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date(),
        createdAt: new Date("2018-10-18"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 2,
        firstName: `Hatranhoang`,
        lastName: ``,
        email: `hatranhoang@gmail.com`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: ``,
        createdAt: new Date("2018-10-15"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 3,
        firstName: `Jiaying`,
        lastName: `Sim`,
        email: `jiaying@ftvlabs.com`,
        phone: `65-90161920`,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date("2018-10-17"),
        createdAt: new Date("2018-10-17"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 4,
        firstName: `Tai.khuu`,
        lastName: ``,
        email: `Tai.khuu@zamo.io`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: ``,
        createdAt: new Date("2018-10-18"),
        avatar: '',
    },
    {
        key: 5,
        firstName: `Thu.nguyen`,
        lastName: `Minh Thu`,
        email: `thu.nguyen@zamo.io`,
        phone: `84-986876009`,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date("2018-10-18"),
        createdAt: new Date("2018-10-18"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 6,
        firstName: `Truyet`,
        lastName: `Nguyen`,
        email: `truyet.nguye@zamo.io`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date(),
        createdAt: new Date("2018-10-18"),
        avatar: '',
    },
]
export default {
  // 支持值为 Object 和 Array
  'GET /api/currentUser': {
    firstName: 'Serati',
    lastName: 'Ma',
    avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    userid: '00000001',
    email: 'antdesign@alipay.com',
    signature: '海纳百川，有容乃大',
    title: '交互专家',
    group: '蚂蚁金服－某某某事业群－某某平台部－某某技术部－UED',
    tags: [
      {
        key: '0',
        label: '很有想法的',
      },
      {
        key: '1',
        label: '专注设计',
      },
      {
        key: '2',
        label: '辣~',
      },
      {
        key: '3',
        label: '大长腿',
      },
      {
        key: '4',
        label: '川妹子',
      },
      {
        key: '5',
        label: '海纳百川',
      },
    ],
    notifyCount: 12,
    country: 'China',
    geographic: {
      province: {
        label: '浙江省',
        key: '330000',
      },
      city: {
        label: '杭州市',
        key: '330100',
      },
    },
    address: '西湖区工专路 77 号',
    phone: '0752-268888888',
  },
  // GET POST 可省略
  'GET /api/users': [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park',
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park',
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park',
    },
  ],
  'POST /api/login/account': (req, res) => {
    const { password, userName, type } = req.body;
    if (password === '888888' && userName === 'admin') {
      res.send({
        status: 'ok',
        type,
        currentAuthority: 'admin',
      });
      return;
    }
    if (password === '123456' && userName === 'user') {
      res.send({
        status: 'ok',
        type,
        currentAuthority: 'user',
      });
      return;
    }
    res.send({
      status: 'error',
      type,
      currentAuthority: 'guest',
    });
  },

  'POST /api/register': (req, res) => {
    res.send({ status: 'ok', currentAuthority: 'user' });
  },
  'GET /api/500': (req, res) => {
    res.status(500).send({
      timestamp: 1513932555104,
      status: 500,
      error: 'error',
      message: 'error',
      path: '/base/category/list',
    });
  },
  'GET /api/404': (req, res) => {
    res.status(404).send({
      timestamp: 1513932643431,
      status: 404,
      error: 'Not Found',
      message: 'No message available',
      path: '/base/category/list/2121212',
    });
  },
  'GET /api/403': (req, res) => {
    res.status(403).send({
      timestamp: 1513932555104,
      status: 403,
      error: 'Unauthorized',
      message: 'Unauthorized',
      path: '/base/category/list',
    });
  },
  'GET /api/401': (req, res) => {
    res.status(401).send({
      timestamp: 1513932555104,
      status: 401,
      error: 'Unauthorized',
      message: 'Unauthorized',
      path: '/base/category/list',
    });
  },
  'GET /api/user/id=:id': (req, res) => {
      const { id } = req.params
      const data = queryArray(database, id)

      if (data) {
        res.status(200).json(data)
      } else {
        res.status(404).json({
            message: 'Not Found',
            documentation_url: 'http://localhost:8000/request',
        })
      }
  },
};
