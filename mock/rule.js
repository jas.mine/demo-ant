import { parse } from 'url';

// mock tableListDataSource
let tableListDataSource = [];

tableListDataSource = [
    {
        key: 0,
        name: `Henry Tran`,
        email: `hery@ftvlabs.com`,
        phone: `(65) 9016 1920`,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date(),
        createdAt: new Date("2018-10-18"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 1,
        name: `Dan Nguyen`,
        email: `nguyenducanh2210@gmail.com`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date(),
        createdAt: new Date("2018-10-18"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 2,
        name: `Hatranhoang`,
        email: `hatranhoang@gmail.com`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: ``,
        createdAt: new Date("2018-10-15"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 3,
        name: `Jiaying Sim`,
        email: `jiaying@ftvlabs.com`,
        phone: `(65) 9016 1920`,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date("2018-10-17"),
        createdAt: new Date("2018-10-17"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 4,
        name: `Tai.khuu`,
        email: `Tai.khuu@zamo.io`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: ``,
        createdAt: new Date("2018-10-18"),
        avatar: '',
    },
    {
        key: 5,
        name: `Thu.nguyen Minh Thu`,
        email: `thu.nguyen@zamo.io`,
        phone: `(84) 986876009`,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date("2018-10-18"),
        createdAt: new Date("2018-10-18"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 6,
        name: `Truyet Nguyen`,
        email: `truyet.nguye@zamo.io`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date(),
        createdAt: new Date("2018-10-18"),
        avatar: '',
    },
]
/* for (let i = 0; i < 7; i += 1) {
  tableListDataSource.push({
    key: i,
    disabled: i % 6 === 0,
    href: 'https://ant.design',
    avatar: [
      'https://gw.alipayobjects.com/zos/rmsportal/eeHMaZBwmTvLdIwMfBpg.png',
      'https://gw.alipayobjects.com/zos/rmsportal/udxAbMEhpwthVVcjLXik.png',
    ][i % 2],
    name: `TradeCode ${i}`,
    title: `Task ${i}`,
    owner: 'jasmine',
    desc: 'description',
    callNo: Math.floor(Math.random() * 1000),
    status: Math.floor(Math.random() * 10) % 4,
    updatedAt: new Date(`2017-07-${Math.floor(i / 2) + 1}`),
    createdAt: new Date(`2017-07-${Math.floor(i / 2) + 1}`),
    progress: Math.ceil(Math.random() * 100),
  });
} */

function getRule(req, res, u) {
  let url = u;
  if (!url || Object.prototype.toString.call(url) !== '[object String]') {
    url = req.url; // eslint-disable-line
  }

  const params = parse(url, true).query;

  let dataSource = tableListDataSource;

  if (params.sorter) {
    const s = params.sorter.split('_');
    dataSource = dataSource.sort((prev, next) => {
      if (s[1] === 'descend') {
        return next[s[0]] - prev[s[0]];
      }
      return prev[s[0]] - next[s[0]];
    });
  }

  if (params.status) {
    const status = params.status.split(',');
    let filterDataSource = [];
    status.forEach(s => {
      filterDataSource = filterDataSource.concat(
        dataSource.filter(data => parseInt(data.status, 10) === parseInt(s[0], 10))
      );
    });
    dataSource = filterDataSource;
  }

  if (params.name) {
    dataSource = dataSource.filter(data => data.name.indexOf(params.name) > -1);
  }

  let pageSize = 10;
  if (params.pageSize) {
    pageSize = params.pageSize * 1;
  }

  const result = {
    list: dataSource,
    pagination: {
      total: dataSource.length,
      pageSize,
      current: parseInt(params.currentPage, 10) || 1,
    },
  };

  return res.json(result);
}

function postRule(req, res, u, b) {
  let url = u;
  if (!url || Object.prototype.toString.call(url) !== '[object String]') {
    url = req.url; // eslint-disable-line
  }

  const body = (b && b.body) || req.body;
  const { method, name, desc, key } = body;

  switch (method) {
    /* eslint no-case-declarations:0 */
    case 'delete':
      tableListDataSource = tableListDataSource.filter(item => key.indexOf(item.key) === -1);
      break;
    case 'post':
      const i = Math.ceil(Math.random() * 10000);
      tableListDataSource.unshift({
        key: i,
        href: 'https://ant.design',
        avatar: [
          'https://gw.alipayobjects.com/zos/rmsportal/eeHMaZBwmTvLdIwMfBpg.png',
          'https://gw.alipayobjects.com/zos/rmsportal/udxAbMEhpwthVVcjLXik.png',
        ][i % 2],
        name: `TradeCode ${i}`,
        title: `一个任务名称 ${i}`,
        owner: '曲丽丽',
        desc,
        callNo: Math.floor(Math.random() * 1000),
        status: Math.floor(Math.random() * 10) % 2,
        updatedAt: new Date(),
        createdAt: new Date(),
        progress: Math.ceil(Math.random() * 100),
      });
      break;
    case 'update':
      tableListDataSource = tableListDataSource.map(item => {
        if (item.key === key) {
          Object.assign(item, { desc, name });
          return item;
        }
        return item;
      });
      break;
    default:
      break;
  }

  const result = {
    list: tableListDataSource,
    pagination: {
      total: tableListDataSource.length,
    },
  };

  return res.json(result);
}

export default {
  'GET /api/rule': getRule,
  'POST /api/rule': postRule,
};
