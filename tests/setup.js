import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

global.XMLHttpRequest = require('w3c-xmlhttprequest').XMLHttpRequest;

Enzyme.configure({ adapter: new Adapter() });
