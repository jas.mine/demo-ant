// Link.react.test.js
import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import TableList from '../../src/pages/List/TableList';

const database = [
    {
        key: 0,
        firstName: `Henry`,
        lastName: `Tran`,
        email: `hery@ftvlabs.com`,
        phone: `65-9016 1920`,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date(),
        createdAt: new Date("2018-10-18"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 1,
        firstName: `Dan`,
        lastName: `Nguyen`,
        email: `nguyenducanh2210@gmail.com`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date(),
        createdAt: new Date("2018-10-18"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 2,
        firstName: `Hatranhoang`,
        lastName: ``,
        email: `hatranhoang@gmail.com`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: ``,
        createdAt: new Date("2018-10-15"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 3,
        firstName: `Jiaying`,
        lastName: `Sim`,
        email: `jiaying@ftvlabs.com`,
        phone: `65-90161920`,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date("2018-10-17"),
        createdAt: new Date("2018-10-17"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 4,
        firstName: `Tai.khuu`,
        lastName: ``,
        email: `Tai.khuu@zamo.io`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: ``,
        createdAt: new Date("2018-10-18"),
        avatar: '',
    },
    {
        key: 5,
        firstName: `Thu.nguyen`,
        lastName: `Minh Thu`,
        email: `thu.nguyen@zamo.io`,
        phone: `84-986876009`,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date("2018-10-18"),
        createdAt: new Date("2018-10-18"),
        avatar: 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png',
    },
    {
        key: 6,
        firstName: `Truyet`,
        lastName: `Nguyen`,
        email: `truyet.nguye@zamo.io`,
        phone: ``,
        status: Math.floor(Math.random() * 10) % 4,
        updatedAt: new Date(),
        createdAt: new Date("2018-10-18"),
        avatar: '',
    },
]
describe('<TableList />', () => {
    it('renders correctly', () => {
        const table = <TableList props={database} />;
        const tree = renderer.create(table).toJSON();
        expect(tree).toMatchSnapshot();
    });

    it('simulates click events', () => {
        const onButtonClick = jest.fn();
        const wrapper = shallow(<TableList props={database} />);
        console.log(wrapper)
        /*wrapper.find('Button').props().onPress();
        expect(onButtonClick).toHaveBeenCalled();*/
    });
});
