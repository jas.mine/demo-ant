module.exports = {
  collectCoverageFrom: ["src/**/*.{js,jsx,mjs}"],
  testMatch: ["<rootDir>/tests/**/*.{js,jsx,mjs}", "<rootDir>/tests/unit/?(*.)(spec|test).{js,jsx,mjs}"],
  transform: { "^.+\\.js$": "babel-jest"},
  transformIgnorePatterns: ["[/\\\\]node_modules[/\\\\].+\\.(js|jsx|mjs)$"]
};
